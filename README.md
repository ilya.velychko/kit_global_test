## Getting Started
To test application you can visit https://kit-global-test.vercel.app/

In order to add product to basket you need to click on "+" button and then press "Add" button.
If you've added products, and then you click "Delete" it will remove that product from basket.
In the header you can see the amount of all products that you have added.
On the /basket page you can see the total price of all products.

I am not using LocalStorage to save current state of /basket page
I am not using Saga to make server request