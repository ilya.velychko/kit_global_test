export interface IProduct {
    _id: string;
    img: string;
    title: string
    description: string;
    price: number;
    quantity: number;
}

export interface IProductMethods {
    addProductToBasket: (product: IProduct, id: string) => void;
    removeProductFromBasket: (id: string) => void;
}