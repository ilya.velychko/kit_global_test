import { IProduct } from "@/interfaces";

export const calculateProductsPrice = (basket: IProduct[]) => {
    return basket.reduce((curr: any, prev: IProduct): number => {
        return (prev.price * prev.quantity) + curr
    }, 0);
}