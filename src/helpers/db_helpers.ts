import { MongoClient } from "mongodb";
export const connectToDB = async () => {
    const url = `mongodb+srv://${process.env.mongodb_username}:${process.env.mongodb_password}@cluster0.2sawjkc.mongodb.net/?retryWrites=true&w=majority`;

    return new MongoClient(url);
}