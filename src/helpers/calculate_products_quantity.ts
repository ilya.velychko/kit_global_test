import { IProduct } from "@/interfaces";

export const calculateProductsQuantity = (products: IProduct[]) => {
    return products?.reduce((curr: number, prev: IProduct): number => {
        return prev.quantity + curr;
    }, 0);
}