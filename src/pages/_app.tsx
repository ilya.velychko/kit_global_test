import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { Header } from "@/components";
import { Providers } from "@/store";
import Head from "next/head";

export default function App({ Component, pageProps }: AppProps) {
  return (
      <>
        <Head>
          <title>Trail Shoes Shop</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <link rel="icon" href="/favicon.ico" />
        </Head>
          <Providers>
              <Header />
              <Component {...pageProps} />
          </Providers>
      </>
  )
}
