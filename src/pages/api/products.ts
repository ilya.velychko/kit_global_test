import type { NextApiRequest, NextApiResponse } from 'next';
import { connectToDB } from "@/helpers/db_helpers";

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
    const client = await connectToDB();

    await client.connect();

    const db = client.db('productsDB');

    const products = await db.collection('products').find({}).toArray();

    await client.close();

    res.status(200).json({
        m: "All products",
        products
    });
}