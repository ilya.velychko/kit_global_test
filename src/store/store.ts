import { configureStore } from "@reduxjs/toolkit";
import productsReducer from "./Features/products/productsSlice";
import basketSlice from "./Features/basket/basketSlice";

export const store = configureStore({
    reducer: {
        products: productsReducer,
        basket: basketSlice
    }
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;