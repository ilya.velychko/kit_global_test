import { createSlice } from "@reduxjs/toolkit";
import { IProduct } from "@/interfaces";

interface ProductsState {
    products: IProduct[]
}

const initialState: ProductsState = {
    products: []
}

export const productsSlice = createSlice({
    name: "products",
    initialState,
    reducers: {
        saveProducts: (state, action) => {
            state.products.push(action.payload);
        }
    }
})

export const {
    saveProducts
} = productsSlice.actions;

export default productsSlice.reducer;