import { createSlice } from "@reduxjs/toolkit";
import { IProduct } from "@/interfaces";

interface ProductsState {
    products: IProduct[]
}

const initialState: ProductsState = {
    products: []
};

export const basketSlice = createSlice({
    name: "basket",
    initialState,
    reducers: {
        // Here we merge products with same ID in the one object to avoid issue with having two same objects in array
        addToBasket: (state, action) => {
            return {
                ...state,
                products: [
                    ...state.products.filter(((product: any) => product._id !== action.payload.id)),
                    action.payload.product
                ]
            }
        },
        // Here we simply remove product from an array of products
        removeFromBasket: (state, action) => {
            return {
                ...state,
                products: state.products.filter((product: any) => product._id !== action.payload)
            }
        }
    }
});

export const { addToBasket, removeFromBasket } = basketSlice.actions;

export default basketSlice.reducer;