import { useState } from "react";
import Image from "next/image";
import styles from "./Product.module.css";
import { IProduct, IProductMethods } from "@/interfaces";
import { useRouter } from "next/router";

export const Product = (props: IProduct & IProductMethods) => {
    const {
        _id,
        title,
        description,
        price,
        img,
        addProductToBasket,
        removeProductFromBasket,
        quantity: productQuantity,
    } = props;

    const [quantity, setQuantity] = useState<number>(productQuantity ?? 0);
    const router = useRouter();

    const isAllProductsPage = router.pathname === '/';

    // I think this method could've been simplified
    // Here I validate quantity value
    // If quantity is less than 0 then we're simply showing an error alert
    // Also we check if we are on /basket page if yes then we can remove product if quantity is 0
    const handleAddProduct = () => {
        if (quantity < 0) {
            alert("Please Select at Least One Product");
            return;
        }
        if (quantity === 0 && !isAllProductsPage) {
            removeProductFromBasket(_id);
            return;
        }
        addProductToBasket({
            _id,
            title,
            description,
            price,
            img,
            quantity
        }, _id)
    }

    const handleRemoveProduct = () => {
        if (quantity <= 0) {
            return;
        }
        removeProductFromBasket(_id);
        setQuantity(0);
    }

    const formattedPrice = `${price}$`

    return (
        <div className={styles.productWrapper}>
            <Image src={`/${img}`} height={300} width={350} alt={title} />
            <br />
            <div className={styles.productInfo}>
                <span className={styles.title}>{title}</span>
                <span className={styles.price}>{formattedPrice}</span>
            </div>
            <div className={styles.productActions}>
                <button onClick={handleAddProduct}>{quantity === 0 && !isAllProductsPage ? "Remove" : "Add"}</button>
                <button onClick={handleRemoveProduct} disabled={quantity <= 0}>Delete</button>
                <div>
                    <button id="remove_product_quantity" onClick={() => setQuantity((prevState) => prevState - 1)} disabled={quantity <= 0}>-</button>
                    <span>{quantity}</span>
                    <button id="add_product_quantity" onClick={() => setQuantity((prevState) => prevState + 1)}>+</button>
                </div>
            </div>
        </div>
    )
}