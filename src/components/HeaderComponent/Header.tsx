import styles from "./Header.module.css";
import Link from "next/link";
import { useRouter } from "next/router";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { calculateProductsQuantity } from "@/helpers/calculate_products_quantity";
export const Header = () => {
    const router = useRouter();
    const products = useSelector((state: RootState) => state.basket.products);

    const productsQuantity = calculateProductsQuantity(products);

    const linkStyles = (activeLink: string) => {
        return router.pathname === activeLink ? `${styles.headerLink} ${styles.active}` : `${styles.headerLink}`;
    }

    return (
        <div className={styles.headerComponentWrapper}>
            <div className={styles.headerNavigationMenu}>
                <Link href="/" className={linkStyles('/')}>Products</Link>
                <Link href="/basket" className={linkStyles('/basket')}>
                    Basket { productsQuantity }
                </Link>
            </div>
        </div>
    )
}