import styles from "./Basket.module.css";
import { useSelector } from "react-redux";
import { RootState } from "@/store";
import { ProductsList } from "@/components";
import { calculateProductsPrice } from "@/helpers/calculate_products_price";

export const Basket = () => {
    const basket = useSelector((state: RootState) => state.basket.products);

    const productsPrice = calculateProductsPrice(basket);

    if (!basket.length) {
        return (
            <div>No Products In Basket</div>
        )
    }

    return (
        <div className={styles.basketComponentWrapper}>
            <div className={styles.price}>Total Price: {productsPrice}$</div>
            <ProductsList
                products={basket}
            />
        </div>
    )
}