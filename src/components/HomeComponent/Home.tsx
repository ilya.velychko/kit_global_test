import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { RootState, saveProducts } from "@/store";
import { ProductsList } from "@/components";

export const Home = () => {
    const products = useSelector((state: RootState) => state.products.products).flat(1);
    const dispatch = useDispatch();

    useEffect(() => {
        if (!products.length) {
            fetch('/api/products')
                .then((response) => response.json())
                .then(({ products }) => {
                    dispatch(saveProducts(products));
                })
                .catch((err) => console.log(err));
        }
    }, [products.length]);

    if (!products.length) {
        return (
            <div>Loading...</div>
        )
    }

    return (
        <ProductsList
            products={products}
        />
    )
}