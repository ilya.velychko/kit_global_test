import { Product } from "@/components";
import styles from "./ProductsList.module.css";
import { useDispatch } from "react-redux";
import { addToBasket, removeFromBasket } from "@/store";
import { IProduct } from "@/interfaces";

// This is a reusable component that we use in Basket component and Home component, handlers are same but the data is different
export const ProductsList = ({ products }: { products: IProduct[] }) => {
    const dispatch = useDispatch();

    // I added dispatch and methods that call that dispatch here
    // because I think it will only create that dispatch once instead of creating dispatch on every Product Component render
    const addProductToBasket = (product: IProduct, id: string) => {
        dispatch(addToBasket({
            product,
            id
        }));
    }

    const removeProductFromBasket = (id: string) => {
        dispatch(removeFromBasket(id))
    }

    if (!products?.length) {
        return <div>Loading...</div>
    }

    return (
        <div className={styles.productsListWrapper}>
            {
                products.map((product: IProduct) => (
                    <div key={product._id}>
                        <Product
                            addProductToBasket={addProductToBasket}
                            removeProductFromBasket={removeProductFromBasket}
                            {...product}
                        />
                    </div>
                ))
            }
        </div>
    )
}